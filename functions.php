<?php
// include the widgets
//include_once('includes/SocialMediaWidget.php');
include_once('includes/geek_profile_comment.php');

/**
 *
 * setups the theme, allowing theme to support features
 *
 *
 *
 */

function geekProfile_setup(){

    // width of the content
    if ( ! isset( $content_width ) ) {
        $content_width = 600;
    }

    // customization of the background color and image
    add_theme_support( 'custom-background' );

    // an image that is chosen as the representative image in the theme top header section
    $header_defaults = array(
        'width'         => 200,
        'height'        => 200,
    );
    add_theme_support( 'custom-header', $header_defaults );

    $html5_defaults = array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'widgets'
    );
    add_theme_support( 'html5', $html5_defaults );

    // add rss feed link in the html head tag
    add_theme_support( 'automatic-feed-links' );

    //    register the navigation menu
    register_nav_menu('main-menu',__( 'Main Menu' ));
    /*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 604, 270, true );

    add_theme_support( "title-tag" );



}

add_action('after_setup_theme', 'geekProfile_setup');

/**
 * @param $more
 * @return string
 *
 * the excerpt
 */
function custom_excerpt_more( $more ) {
    return "<p class='text-right'>
                <a href='".get_permalink(get_the_ID())."'>
                        <button class='btn btn-primary'>".__('read more','geek_profile' )."</button>
                </a>
            </p>";
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 *
 * enqueue scripts and styles
 */
function geekProfile_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'site', get_template_directory_uri().'/assets/js/site.js', array('jquery') );
}

add_action( 'wp_enqueue_scripts', 'geekProfile_scripts' );

/**
 *  creates a sidebar on the admin panel
 */
function theme_slug_widgets_init() {

    register_sidebar(array(
        'name' => __( 'Left Sidebar', 'geek_profile' ),
        'id' => 'left_sidebar'
    ) );

}
add_action( 'widgets_init', 'theme_slug_widgets_init' );


/**
 * Creates custom sections to set social media username and profile image
 *
 * @param $wp_customize
 */
function geek_profile_customize_register( $wp_customize ) {
    // settings
    $wp_customize->add_setting( 'facebook_settings' , array(
        'default'     => 'enter username',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_setting( 'twitter_settings' , array(
        'default'     => 'enter username',
        'transport'   => 'refresh',
    ) );


    // sections
    $wp_customize->add_section( 'social_media_section' , array(
        'title'      => __( 'Social Media', 'geek_profile' ),
        'priority'   => 90,
    ) );

    // Controls
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
        'label'        => __( 'Facebook', 'geek_profile' ),
        'section'    => 'social_media_section',
        'settings'   => 'facebook_settings',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
        'label'        => __( 'Twitter', 'geek_profile' ),
        'section'    => 'social_media_section',
        'settings'   => 'twitter_settings',
    ) ) );
}
add_action( 'customize_register', 'geek_profile_customize_register' );
