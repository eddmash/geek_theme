<?php
/**
 *
 *Template Name: Profile Page
*/
?>
<?php

// includes the header file
get_header();
?>

	<div class="row">

		<?php

		// includes the sidebar file with the navigation
		get_sidebar();
		?>

		<div class="col-sm-9">

			<div id="content">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h4><?php the_title(); ?></h4>
					<p><?php the_content(); ?></p>

				<?php endwhile; else : ?>

					<p><?php _e( 'Sorry, no posts matched your criteria.', 'geek_profile' ); ?></p>

				<?php endif; ?>
			</div>
		</div>
	</div>


<?php

// includes the footer file
get_footer();
?>