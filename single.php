<?php

    // includes the header file
    get_header();
?>

    <div class="row">

        <?php

            // includes the sidebar file with the navigation
            get_sidebar();
        ?>

        <div class="col-sm-9">

            <div id=" row">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div id="post-<?php the_ID(); ?>" <?php post_class(array('content','col-sm-12')); ?>>
                        <h4><?php the_title(); ?></h4>
                        <p class="post-meta clearfix">
                            <span class="posted_by ">
                                <span class="label label-default"><?php _e("Posted By ","geek-profile"); ?>:&nbsp;</span>
                                <?php the_author_posts_link();?>
                            </span>
                            <span class="posted_on">
                                <span class="label label-default"><?php _e(' ON ','geek_profile'); ?>:&nbsp;</span>
                                <?php the_time( get_option( 'date_format' ) ); ?>
                            </span>
                            <span class="pull-right edit_post_link">
                                <?php edit_post_link(__('Edit','geek_profile'));?>
                            </span>
                        </p>

                        <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail();
                            }
                            the_content();
                        ?>

                        <?php wp_link_pages(); ?>
                    </div>

                    <div class="tags-categories col-sm-12">
                        <p class="small "><?php the_tags(); ?></p>

                        <p class="small">
                            <?php _e('Categories: ','geek_profile').'$nbps;'.the_category(' , '); ?>
                        </p>
                    </div>


                <?php
                endwhile;
                    get_template_part("includes/pagination-single");
                    comments_template();

                else : ?>

                    <p><?php _e( 'Sorry, no posts matched your criteria.','geek_profile' ); ?></p>

                <?php endif; ?>
            </div>
        </div>
    </div>


<?php

    // includes the footer file
    get_footer();
?>