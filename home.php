<?php

    // includes the header file
    get_header();
?>

    <div class="row">

        <?php

            // includes the sidebar file with the navigation
            get_sidebar();
        ?>

        <div class="col-sm-9">

            <div id="content">
                <h3><?php wp_title(''); ?></h3>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div <?php post_class(array("blog-summary","well","well-sm" )); ?> id="post-<?php the_ID(); ?>">
                        <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

                        <p class="post-meta">
                            <span class="posted_by ">
                                <span class="label label-default"><?php _e("Posted By ","geek-profile"); ?>:&nbsp;</span>
                                <?php the_author_posts_link();?>
                            </span>
                        </p>

                        <?php the_excerpt();?>
                    </div>

                <?php
                    endwhile;

                    // add pagination
                    get_template_part("includes/pagination");

                    else :?>

                    <p><?php _e( 'Sorry, no posts matched your criteria.' ,'geek_profile' ); ?></p>

                <?php endif; ?>
            </div>
        </div>
    </div>


<?php

    // includes the footer file
    get_footer();
?>