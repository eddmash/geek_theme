<div class="row">
<?php
    if(comments_open()): // check if comments are open?>

        <div class=" col-sm-12 comment-form-greek-profile">
            <?php comment_form(); // displays the comment form?>
        </div>

        <div class="comment-list-greek-profile col-sm-12">
            <h2>
                <?php
                    printf(_n('There is one comment ','There are %d comments',get_comments_number(),'greek_profile'),
                    get_comments_number()); // displays number of comments
                ?>
            </h2>
            <?php wp_list_comments('type=comment&callback=geek_profile_comment'); // display the list of comments ?>
        </div>

        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // check for comment navigation ?>

            <div class="col-sm-12">

                <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">

                    <ul class="pager">

                        <li class="nav-previous">
                            <?php previous_comments_link( __( '&larr; Older Comments', 'geek_profile' ) ); ?>
                        </li>

                        <li class="nav-next">
                            <?php next_comments_link( __( 'Newer Comments &rarr;', 'geek_profile' ) ); ?>
                        </li>

                    </ul>

                </nav><!-- #comment-nav-below -->

            </div>

        <?php endif; // Check for comment navigation. ?>

    <?php endif; // check if comments are open ?>
</div>