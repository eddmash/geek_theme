<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 3/29/15
 * Time: 9:43 AM
 */

/**
 * @param $comment
 * @param $args
 * @param $depth
 *
 * the comment list setup
 */
function geek_profile_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }

    // find out if comment has children
    $class="";
    if($args['has_children']):
        $class = 'has_children';
    endif;

?>


        <div id="div-comment-<?php comment_ID() ?>" class="comment-body panel-default panel">

            <div class="comment-author vcards panel-heading clearfix">
                <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
                <?php
                    printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'),
                        get_comment_author_link())
                ?>
                <span class="pull-right geek-profile-comment-meta">
                    <small>
                        <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                            <?php
                            /* translators: 1: date, 2: time */
                                printf( __('posted on %1$s at %2$s','geek_profile'),
                                    get_comment_date(),  get_comment_time()) ;
                            ?>
                        </a>
                    </small>
                    <small>
                        <?php edit_comment_link(__('(Edit)','geek_profile'),'  ','' );?>
                    </small>
                </span>
            </div>


            <div class="panel-body">

                <?php if ($comment->comment_approved == '0') : ?>
                    <p>
                        <em class="comment-awaiting-moderation">
                            <?php _e('Your comment is awaiting moderation.','geek_profile') ?>
                        </em>
                    </p>
                <?php
                    elseif($comment->comment_approved):
                        comment_text();

                    endif;
                ?>
            </div>

            <div class="panel-footer ">
				<span class="geek-profile-comment-reply-link">
					<?php
                        comment_reply_link(
                            array_merge( $args,
                                array('add_below' => $add_below,
                                    'depth' => $depth,
                                    'max_depth' => $args['max_depth'])));
                    ?>
				</span>
            </div>


        </div>





<?php }

function greek_profile_comment_form($args) {
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $args['fields'] = array(
        'author' =>
            '<div class="form-group">
            <label for="author">'.__("Name","greek_profile").'</label>
            <input id="author"
                    name="author" type="text" value="' . __( 'Your Name ...', 'geek_profile' ) .'"
                    size="30"' . ( $req ? " aria-required='true'" : '' ) . '
                    class="form-control"
                    placeholder="' . __( 'Your Name', 'geek_profile' ) . ( $req ? '*' : '' ) . '" /></div>',

        'email' =>
            '<div class="form-group">
            <label for="email">'.__("Email","greek_profile").'</label>
            <input id="email"
                    name="email" type="text" value="' . __( 'Your Email ...', 'geek_profile' ) .'"
                    size="30"' . ( $req ? " aria-required='true'" : '' ) . '
                    class="form-control"
                    placeholder="' . __( 'Your Email', 'nonewz' ) . ( $req ? '*' : '' ) . '" /></div>',
    );
    $args['comment_notes_before'] = "";
    $args['comment_notes_after'] = "";
    $args['label_submit'] = "Submit";
    $args['class_submit'] = "btn btn-primary";
    $args['title_reply'] = __("Leave a Comment","greek_profile");
    $args['comment_field'] = '<div class="form-group">
                            <label for="comment">'.__("Message","greek_profile").'</label>
                            <textarea id="comment"
                                            name="comment"
                                            aria-required="true"
                                            class="form-control"
                                            placeholder="'. __( 'Your Message here...', 'geek_profile' ) .'"
                                            tabindex="4"></textarea></div>';

    return $args;
}

add_filter('comment_form_defaults', 'greek_profile_comment_form');





