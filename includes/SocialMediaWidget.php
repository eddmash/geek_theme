<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 3/29/15
 * Time: 2:00 AM
 */

class SocialMediaWidget extends WP_Widget{

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        // widget actual processes
        parent::__construct(
            'SocialMediaWidget', // Base ID
            __( 'Social Media', 'geek_profile' ), // Name
            array( 'description' => __( 'Set the social media links', 'geek_profile' ), ) // Args
        );

    }
    /**
     * Front-end display of widget.
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        // outputs the content of the widget
    }

    /**
     * Back-end widget form.
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // set title
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'geek_title' ); ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">
                <?php _e( 'Title:','geek_profile' ); ?>
            </label>

            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>"
                   type="text" value="<?php echo esc_attr( $title ); ?>" >
        </p>

<?php }

}

add_action( 'widgets_init', function(){
    register_widget( 'SocialMediaWidget' );
});