<?php

    // includes the header file
    get_header();
?>

    <div class="row">

        <?php

            // includes the sidebar file with the navigation
            get_sidebar();
        ?>

        <div class="col-sm-9">

            <div id="content">

                <h3>
                    <?php

                        if(is_front_page()):
                            _e("home","geek_profile");
                        else:
                            wp_title('');
                        endif;
                    ?>
                </h3>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <h4><?php the_title(); ?></h4>
                    <p><?php the_content(); ?></p>

                <?php endwhile; else : ?>

                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'geek_profile' ); ?></p>

                <?php endif; ?>

            </div>
        </div>
    </div>


<?php

    // includes the footer file
    get_footer();
?>