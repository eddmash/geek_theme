<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 3/27/15
 * Time: 3:29 PM
 */
?>
    <div class="row">
        <footer>
            <div class="col-sm-offset-3 col-sm-4"> &copy; 2015 eddmash designs </div>
            <div class="col-sm-4">
                <ul class="nav nav-pills" id="social-media">
                    <li>
                        <a href="
                            <?php
                                echo esc_url('http://twitter.com/').esc_attr(get_theme_mod('twitter_settings'))
                            ?>"
                            target="_blank"
                            >
                            <?php _e("Twitter","geek_profile")?>
                        </a>
                    </li>
                    <li>
                        <a href="
                            <?php
                                 echo esc_url('http://facebook.com/').esc_attr(get_theme_mod('facebook_settings'))
                            ?>"
                           target="_blank">
                            <?php _e("Facebook","geek_profile")?>
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    </div>

    </div>

    <?php
        // plugins generally use this hook to reference JavaScript files
        wp_footer();
    ?>

</body>
</html>