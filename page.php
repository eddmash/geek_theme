<?php

    // includes the header file
    get_header();
?>

    <div class="row">

        <?php

            // includes the sidebar file with the navigation
            get_sidebar();
        ?>

        <div class="col-sm-9">

            <div id="row">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div id="post-<?php the_ID(); ?>" <?php post_class(array('content','col-sm-12')); ?>>
                        <h4><?php the_title(); ?></h4>
                        <p class="post-meta clearfix">
                            <span class="pull-right edit_post_link">
                                <?php edit_post_link(__('Edit','geek_profile'));?>
                            </span>
                        </p>
                        <p><?php the_content(); ?></p>
                    </div>

                <?php endwhile;
                    comments_template();
                else : ?>

                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'geek_profile' ); ?></p>

                <?php endif; ?>

            </div>
        </div>
    </div>


<?php

    // includes the footer file
    get_footer();
?>