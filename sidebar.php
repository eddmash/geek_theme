<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 3/27/15
 * Time: 3:40 PM
 */
?>

<div class="col-sm-3" id="sidebar">
    <div class="row">
        <div class="col-sm-12 text-center" id="logo" >


            <img src="<?php header_image(); ?>"
                 height="<?php echo get_custom_header()->height; ?>"
                 width="<?php echo get_custom_header()->width; ?>"
                 alt="the profile picture"
                 class="img-circle"/>
            <h4><?php bloginfo("name")?></h4>
        </div>

        <div class="col-sm-12">
            <?php
                $defaults = array(
                    'theme_location'  => 'main-menu',
                    'container'       => 'div',
                    'container_id'    => 'navigation',
                    'menu_class'      => 'nav nav-pills nav-stacked',
                    'menu_id'         => 'nav-menu',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'depth'           => 1,
                );
                wp_nav_menu($defaults);

            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
                if(!dynamic_sidebar('left_sidebar')):

//                    dynamic_sidebar();

                endif;
            ?>
        </div>
    </div>
</div>