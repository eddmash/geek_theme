<?php

    // includes the header file
    get_header();
?>

    <div class="row">

        <?php

            // includes the sidebar file with the navigation
            get_sidebar();
        ?>

        <div class="col-sm-9">

            <div id="row">

                <h3>
                    <?php

                        $message = "";
                        if (is_category()):
                            $message=__("showing category", 'geek_profile');
                        elseif(is_tag()):
                            $message=__("showing tag", 'geek_profile');
                        elseif(is_author()):
                            $message=__("Posts Authored By ", 'geek_profile');
                        endif;

                        echo $message.'&nbsp;<em>"'.wp_title('',false).' "</em>&nbsp; Results ';
                    ?>
                </h3>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(array("blog-summary","well","well-sm" )); ?>>
                        <h4><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h4>
                        <p><?php the_excerpt(); ?></p>

                        <?php wp_link_pages(); ?>
                    </div>

                <?php endwhile;

                    // add pagination
                    get_template_part("includes/pagination");


                else :?>

                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'geek_profile' ); ?></p>

                <?php endif; ?>
            </div>
        </div>
    </div>


<?php

    // includes the footer file
    get_footer();
?>